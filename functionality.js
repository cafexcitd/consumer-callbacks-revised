//SCRIPT BY KARAN FOR CAFEX

//This script contains all the functionality for the page which you can see down there, for instance the callbacks column of the page,
// starting the call, ending the call, showing annotation information, etc.

$( document ).ready(function() { 														// this function displays agent list onclick of start support button
document.getElementById('go').onclick = function(event) { 
        $('.multipleAgents').css("display","block");
    };
    document.getElementById('end').onclick = function(event) {				//this function ends the call, hidding the pause and resume cobrowse buttons
        $('#cobrowseControls').css("display","none");
        AssistSDK.endSupport();
    };
    document.getElementById('pauseCobrowse').onclick = function(event) {	//this function pauses the cobrowse
        AssistSDK.pauseCobrowse();
        $('.showCallbacks').text('Cobrowse has been paused');
        $('.currentCallbackMethod').text('AssistSDK.pauseCobrowse');
    };
    document.getElementById('resumeCobrowse').onclick = function(event) {	//this function resumes the paused cobrowse session
        AssistSDK.resumeCobrowse();
        $('.showCallbacks').text('Cobrowse has been resumed');
        $('.currentCallbackMethod').text('AssistSDK.resumeCobrowse');
    };
    $('#annotationToggle').click(function() {								//this function toggles the annotation information
        $('#annodiv').toggle();
    });
AssistSDK.onWebcamUseAccepted = function() {								//this function checks whether the webcam permission is granted or not
        $('.showCallbacks').text('You granted cam permission');
        $('.currentCallbackMethod').text('AssistSDK.onWebcamUseAccepted');
    }
    AssistSDK.onConnectionEstablished = function() {						//confiramtion when the call is first accepted by agent and mark session as active through green blinking dot
        $('.showCallbacks').text('call accepted by the agent');
        $('.currentCallbackMethod').text('AssistSDK.onConnectionEstablished');
        $('#GreenIsGo').css("visibility","visible");
        $('#RedIsNo').css("visibility","hidden");
    };
    AssistSDK.agentRequestedCobrowse = function() {							//fired when agent requests a cobrowse
        $('shwoCallbacks').text('agent requested cobrwose');
    }
    AssistSDK.onCobrowseActive = function() {								//shows the actice cobrowse
        $('.showCallbacks').text('cobrowse active');
        $('.currentCallbackMethod').text('AssistSDK.onCobrowseActive');
        $('#cobrowseControls').css("display","block");
    };
    AssistSDK.onAnnotationAdded = function() {								//makes annotation block active when agent puts the same on cobrowse session and displays the info about annotations
        $('#annotationInformation').css("display","block");
        $('.showCallbacks').text('agent is adding annotations in page');
        $('.currentCallbackMethod').text('AssistSDK.onAnnotationAdded');
        $('.annotationsInfo').css("display", "block");
        AssistSDK.onAnnotationAdded = function(annotation, agent) {
            $('.annotationsStroke').text('Stroke type of annotation is : ' + annotation.stroke);
            $('.annotationsOpacity').text('Opacity of annotation is : ' + annotation.strokeOpacity);
            $('.annotationsWidth').text('Width of annotation is : ' + annotation.strokeWidth);
            $('.annotationsLength').text('path array length of annotation is : ' + annotation.points.length);
        };
        AssistSDK.onAnnotationsCleared = function() {						//fired when agent erases the annotations 
            $('.showCallbacks').text('agent has cleared the annotations in page');
        };
    };
    AssistSDK.onPushRequest = function(allow, deny) {						//complete document related function, whether to accept document from agent or not, display relative inforamtion along with doc if accepted
        var confirmation = confirm('the agent is trying to show a document, would you like to view the same?');
        if (confirmation) {
            allow();
            $('.showCallbacks').text('accpeted the document to view');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and allow()');
            AssistSDK.onDocumentReceivedSuccess = function() {
                $('.showCallbacks').text('document recieved successfully');
                $('.currentCallbackMethod').text('AssistSDK.onDocumentReceivedSuccess');
            };
            AssistSDK.sharedDocument.onClosed = function() { 				//for informattion when pushed document is closed
                $('.showCallbacks').text('document closed');
                $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
            };
        } else {
            deny();															//continuation...fired when consumer rejects the doc, shows related messages
            $('.showCallbacks').text('rejected the document');
            $('.currentCallbackMethod').text('AssistSDK.onPushRequest and deny()');
        }
    };
    AssistSDK.onError = function(error) {									//shows internet connection error in case of error
        $('.showCallbacks').text('Please check the internet connection as the session is experiecing some connectivity errors');
    }
    AssistSDK.onEndSupport = function() {									//for ending the call, switching back to red status i.e. inactive call
        $('.showCallbacks').text('call has ended');
        $('.currentCallbackMethod').text('AssistSDK.onEndSupport');
        $('#GreenIsGo').css("visibility","hidden");
        $('#RedIsNo').css("visibility","visible");
    };
        var requestCobrowse = function() {									//display message in console when cobrowse is requested and fire screen share request function
        console.log("Co-browse requested");
        // window.displayAlert(translations.awaitingCobrowseResponse, "alert-info", "cobrowse-alert");
        AssistAgentSDK.requestScreenShare();
    };
	});