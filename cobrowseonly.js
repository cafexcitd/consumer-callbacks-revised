//SCRIPT BY KARAN FOR CAFEX

//This code is used for making a cobrowse only call (directly starting a cobrowse session without actually calling, basically with the 
// help of correlation id)

$(document).ready(function(){
var $codeDisplay = $('#short-code');
        $('#cobrowseonly').click(function (event) {
		event.preventDefault();

		// Retrieve a short code
		$.ajax('https://192.168.4.30:8443/assistserver/shortcode/create', {
			method: 'put',
			dataType: 'json'
		})
		.done(function (response) {

			// Retrieve session configuration (session token, CID etc.)
			$.get('https://192.168.4.30:8443/assistserver/shortcode/consumer', {
				appkey: response.shortCode
			}, function (config) {

				// translate the config retrieved from the server, into
				// parameters expected by AssistSDK
				var params = {
					cobrowseOnly: true,
					correlationId: config.cid,
					scaleFactor: config.scaleFactor,
					sessionToken: config['session-token']
				};

				// start the support session
				AssistSDK.startSupport(params);

				// display the code for the consumer to read to the agent
				$codeDisplay.text(response.shortCode);
			}, 'json');
		});
	});


	// configure Assist to automatically approve share requests
	AssistSDK.onScreenshareRequest = function () {
		return true;
	};

	// the $endSupport button will be hidden
	// $endButton.hide().css('display', 'inline');

	// when screenshare is active, toggle the visibility of the 'end' button

});