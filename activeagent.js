//SCRIPT BY KARAN FOR CAFEX

//This js file is being used for agent selection and animation. When we try to call in particular agent , the animation and selection 
// is defined below. The active agent gets slected, hiding others and then you can call in requested agent. 

//Though it works fine, but I find a long process, please feel free to suggest the shorter methods or anyhow we can shorten this code

$( document ).ready(function() {
document.getElementById('go1').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent1',
            videoMode: "full"
        });
        $('#agent2new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent1new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go2').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent2new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go3').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent3new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go4').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent4new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };


    document.getElementById('go5').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent5new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go6').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent6new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go7').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent7new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };


    document.getElementById('go8').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent8new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };

    document.getElementById('go9').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent9new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };


    document.getElementById('go10').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent10new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };


    document.getElementById('go11').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent12new').css("display","none");
        $('#agent11new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };



    document.getElementById('go12').onclick = function(event) {
        AssistSDK.startSupport({
            destination: 'agent2',
            videoMode: "full"
        });
        $('#agent1new').css("display","none");
        $('#agent3new').css("display","none");
        $('#agent4new').css("display","none");
        $('#agent5new').css("display","none");
        $('#agent6new').css("display","none");
        $('#agent7new').css("display","none");
        $('#agent8new').css("display","none");
        $('#agent9new').css("display","none");
        $('#agent10new').css("display","none");
        $('#agent11new').css("display","none");
        $('#agent2new').css("display","none");
        $('#agent12new').removeClass('col-lg-2 col-md-2 col-sm-3 col-xs-6').addClass('col-lg-12 col-md-12 col-sm-12 col-xs-12 afterAdded');
    };
});